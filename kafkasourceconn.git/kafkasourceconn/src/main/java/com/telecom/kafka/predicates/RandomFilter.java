package com.telecom.kafka.predicates;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.transforms.predicates.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RandomFilter<R extends ConnectRecord<R>> implements Predicate<R> {

	private static final Logger log = LoggerFactory.getLogger(RandomFilter.class);
	
	private double probability = 1.0d;

	@Override
	public void configure(Map<String, ?> configs) {
		
		Object p = configs.get("probability");
		if(p != null) {
			try {
				probability = Double.parseDouble(String.valueOf(p));
			} catch(NumberFormatException nfe) {
				log.error("Error: Value {} given for probability is invalid. Must be a positive double value.", p);
			}
		}
		log.info("RandomFilter predicate configured with probability of {}.", this.probability);
	}

	@Override
	public ConfigDef config() {
		return new ConfigDef().define("probability", Type.DOUBLE, this.probability, Importance.LOW, "Probability of sending a message.");
	}

	@Override
	public boolean test(R record) {
		double r = ((ThreadLocalRandom.current().nextInt(100)+1)/100.0d);
		boolean testOK = (this.probability == 1.0d || r <= this.probability);
		if(log.isDebugEnabled()) 
			log.debug("RandomFilter test (prob={}, r={}) result={}.", this.probability, r, testOK);
		return testOK;
	}

	@Override
	public void close() {
		// Nothing to do
	}

}
