package com.telecom.kafka.source;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.source.SourceTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaSourceTask extends SourceTask {

	private static final Logger log = LoggerFactory.getLogger(KafkaSourceTask.class);
	
	private Object mutex = new Object();
	
	String topicSuffix = "";
	String topicPrefix = "";
	
	List<Pattern> topicRegexPatterns = new ArrayList<Pattern>();
	KafkaConsumer<byte[], byte[]> consumer;
	
	List<String> topics = new ArrayList<String>();
	Map<String, Object> configs = new HashMap<String, Object>();  
	
	ExecutorService executor = Executors.newFixedThreadPool(1);
	private long pollTimeout = 5000L;
	private long retryTimeout = pollTimeout*4;
	//Exchanger<Integer> exchanger = new Exchanger<Integer>();

	private volatile boolean stopping = false;

	private boolean faultTolerant = true;

	//PollTask kafkaTask = new PollTask();
	
	@Override
	public String version() {
		return KafkaSourceConnector.VERSION;
	}

	private KafkaConsumer<byte[], byte[]> getConsumer() {
		
		if(this.consumer != null)
			return this.consumer;

		synchronized(this.mutex) {
			try {
				this.consumer = new KafkaConsumer<byte[], byte[]>(configs);
		
				Map<String, List<PartitionInfo>> topicList = this.consumer.listTopics();
				
				for(Entry<String, List<PartitionInfo>> topicEntry : topicList.entrySet()) {
					for(Pattern pat : topicRegexPatterns) {
						if(pat.matcher(topicEntry.getKey()).matches()) {
							topics.add(topicEntry.getKey());
						}
					}
				}
				this.consumer.subscribe(topics);
				return this.consumer;
			} catch(Exception e) {
				  log.error("Error while creating consumer.", e);
				  throw e;
			}
		}
	}
	
	private void closeConsumer() {
		if(this.consumer != null)
	        synchronized (this.mutex) {
	            Utils.closeQuietly(this.consumer, "consumer", new AtomicReference<>());
	            this.consumer = null;
	        }
	}
	
	@Override
	public void start(Map<String, String> props) {
		stopping = false;

		for(Entry<String, String> entry : props.entrySet()) {
			if(entry.getKey().equals("topics-regex")) {
				this.topicRegexPatterns.clear();
				for(String bs : entry.getValue().split(",")) {
					this.topicRegexPatterns .add(Pattern.compile(bs.trim()));
				};
			} else
			if(entry.getKey().equals(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG)) 
				this.configs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, entry.getValue().equalsIgnoreCase("true"));
			else
			if(entry.getKey().equals("retry-timeout")) 
				this.retryTimeout = Long.parseLong(entry.getValue());
			else
			if(entry.getKey().equals("fault-tolerant")) 
				this.faultTolerant  = Boolean.parseBoolean(entry.getValue());
			else
			if(entry.getKey().equals("poll-timeout")) 
				this.pollTimeout = Long.parseLong(entry.getValue());
			else
			if(entry.getKey().equals("topics-suffix")) 
				this.topicSuffix = entry.getValue();
			else
				if(entry.getKey().equals("topics-prefix")) 
					this.topicPrefix = entry.getValue();
			else
				this.configs.put(entry.getKey(), entry.getValue());
		}
		
		//this.getConsumer();
	}

	@Override
	public void stop() {

        log.info("Stopping kafka source task.");
        stopping = true;
        if (this.consumer != null) {
            this.consumer.wakeup();
			try {
				this.executor.awaitTermination(1, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				log.error("Error while stopping kafka consumer task.", e);
			}
			this.closeConsumer();
        }
	}
	
	class PollTask implements Callable<List<SourceRecord>> {

//		ArrayList<SourceRecord> records = new ArrayList<>(200);
		
		@Override
		public List<SourceRecord> call() {
		      if (KafkaSourceTask.this.stopping) {
		            return null;
		      }
			  try {
				    ConsumerRecords<byte[], byte[]> consumerRecords = KafkaSourceTask.this.getConsumer().poll(Duration.ofMillis(pollTimeout));
				    
				    if (consumerRecords.isEmpty())
		                return Collections.emptyList();
				    
			        if(log.isDebugEnabled())
			        	log.debug("Polled {} records from {}.", consumerRecords.count(), consumerRecords.partitions());
				    
				    ArrayList<SourceRecord> records = new ArrayList<>(consumerRecords.count());

			        for(ConsumerRecord<byte[], byte[]> consumerRecord : consumerRecords) {
				        Map<String, Integer> sourcePartition = Collections.singletonMap("partition", consumerRecord.partition());
				        Map<String, Long> sourceOffset = Collections.singletonMap("offset", consumerRecord.offset());
						  
				        if(log.isTraceEnabled())
							log.trace("Key={}\nValue={}", consumerRecord.key()!=null?new String(consumerRecord.key()):"<null>", new String(consumerRecord.value()));
						  
						if(log.isDebugEnabled())
				        	log.debug("Record sizes: Key {} bytes, Value {} bytes.", consumerRecord.serializedKeySize(), consumerRecord.serializedValueSize());
						
						if(consumerRecord.key() != null)
					        records.add(new SourceRecord(sourcePartition, sourceOffset, topicPrefix+consumerRecord.topic()+topicSuffix, Schema.BYTES_SCHEMA, consumerRecord.key(), Schema.BYTES_SCHEMA, consumerRecord.value()));
						else 
					        records.add(new SourceRecord(sourcePartition, sourceOffset, topicPrefix+consumerRecord.topic()+topicSuffix, Schema.BYTES_SCHEMA, consumerRecord.value()));
						  
				    }
				    return records;
				    
				  } catch (Exception e) {
					  if (KafkaSourceTask.this.stopping) {
				            return null;
				      }
					  if(faultTolerant) {
						  log.error("Error while polling records: {}", e.getMessage());
						  log.error("Conection will retried in {} milliseconds:", retryTimeout);
						  try {
							  Thread.sleep(retryTimeout);

						} catch (InterruptedException e1) {
							log.error("Error while waiting for retry: {}", e1.getMessage());
						} finally {
							KafkaSourceTask.this.closeConsumer();
						}
					  } else
						  throw e;
				  }
			return null;
		}
		
	}
	
	PollTask pollTask = new PollTask();
	
	@Override
	public List<SourceRecord> poll() throws InterruptedException {
		
		Future<List<SourceRecord>> futureResult = this.executor.submit(this.pollTask);
		
		try {
			return futureResult.get();
		} catch (CancellationException | ExecutionException e) {
		    log.error("Error while polling records: {}", e.getMessage());
			throw new RuntimeException("Error while polling records:", e);
		}
	}

	
	@Override
    public void commit() throws InterruptedException {
		if(this.consumer!= null) {
		
			Future<Boolean> commitResult = this.executor.submit(new Callable<Boolean>() {

				@Override
				public Boolean call() throws Exception {
					try { 
						KafkaSourceTask.this.consumer.commitSync();
					} catch(java.lang.IllegalStateException e) {
						log.debug("Error while committing.", e);
					}
					return true;
				}
				
			});

			try {
				commitResult.get();
			} catch (CancellationException | ExecutionException e) {
				log.error("Error while commiting.", e);
				throw new RuntimeException("Error while committing: ", e);
			}
		}
	}
	
}
