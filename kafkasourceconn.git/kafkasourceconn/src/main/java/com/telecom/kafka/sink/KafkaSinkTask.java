package com.telecom.kafka.sink;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.utils.Utils;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.sink.SinkTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaSinkTask extends SinkTask {

	private static final Logger log = LoggerFactory.getLogger(KafkaSinkTask.class);
	
	private Object mutex = new Object();
	
	String topicSuffix = "";
	String topicPrefix = "";
	Boolean enableProducerIdempotence = true;
	
	KafkaProducer<byte[], byte[]> producer;
	
	List<String> topics = new ArrayList<String>();
	Map<String, Object> configs = new HashMap<String, Object>();  
	
	private long retryTimeout = 20000L;

	private volatile boolean stopping = false;

	private boolean faultTolerant = true;

	@Override
	public String version() {
		return KafkaSinkConnector.VERSION;
	}

	private KafkaProducer<byte[], byte[]> getProducer() {
		
		if(this.producer != null)
			return this.producer;

		synchronized(this.mutex) {
			try {
				
				configs.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, (this.enableProducerIdempotence)?"true":"false");
				
				log.info("Creating producer. Idempotence={}", configs.get(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG) );
				
				this.producer = new KafkaProducer<byte[], byte[]>(configs);

				return this.producer;
			} catch(Exception e) {
				  log.error("Error while creating producer.", e);
				  throw e;
			}
		}
	}
	
	private void closeProducer() {
		if(this.producer != null)
	        synchronized (this.mutex) {
	            Utils.closeQuietly(this.producer, "producer", new AtomicReference<>());
	            this.producer = null;
	        }
	}
	
	@Override
	public void start(Map<String, String> props) {
		stopping = false;

		for(Entry<String, String> entry : props.entrySet()) {
//			if(entry.getKey().equals("topics-regex")) {
//				this.topicRegexPatterns.clear();
//				for(String bs : entry.getValue().split(",")) {
//					this.topicRegexPatterns .add(Pattern.compile(bs.trim()));
//				};
//			} else
//			if(entry.getKey().equals(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG)) 
//				this.configs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, entry.getValue().equalsIgnoreCase("true"));
//			else
			if(entry.getKey().equals("retry-timeout")) 
				this.retryTimeout = Long.parseLong(entry.getValue());
			else
			if(entry.getKey().equals("fault-tolerant")) 
				this.faultTolerant  = Boolean.parseBoolean(entry.getValue());
			else
			if(entry.getKey().equals("retry-timeout")) 
				this.retryTimeout = Long.parseLong(entry.getValue());
			else
			if(entry.getKey().equals("topics-suffix")) 
				this.topicSuffix = entry.getValue();
			else
				if(entry.getKey().equals("topics-prefix")) 
					this.topicPrefix = entry.getValue();
			else
				if(entry.getKey().equals("enable-producer-idempotence")) 
					this.enableProducerIdempotence = Boolean.parseBoolean(entry.getValue());
			else
				this.configs.put(entry.getKey(), entry.getValue());
		}

	}

	@Override
	public void stop() {

        log.info("Stopping kafka sink task.");
        stopping = true;
        if (this.producer != null) {
			try {
				this.closeProducer();
			} catch (Exception e) {
				log.error("Error while stopping kafka consumer task.", e);
			}
        }
	}
	
	
	class SinkCallback implements Callback {

		@Override
		public void onCompletion(RecordMetadata metadata, Exception exception) {
			if(exception != null)
				log.error("Error while sending records {}", exception);
			else
				if(log.isDebugEnabled())
					log.debug("Record sent to: {}", metadata.toString());
		
		}
		
	}

	SinkCallback sinkCallback = new SinkCallback();

	@Override
	public void put(Collection<SinkRecord> records) {
	      if (this.stopping) {
	            return;
	      }
	      
		  try {
			  
			  KafkaProducer<byte[], byte[]> prod = this.getProducer();
			  int recordsSent = 0;
			  for(SinkRecord r : records) {
				  
				  String topic = r.topic();
				  
				  if (this.topicSuffix != null && this.topicSuffix.length() > 0 && topic.endsWith(this.topicSuffix)) {
					  topic = topic.substring(0, topic.length() - this.topicSuffix.length());
					}
				  
				  ProducerRecord<byte[], byte[]> producerRecord = new ProducerRecord<byte[], byte[]>(topicPrefix + topic, (byte[])r.key(), (byte[])r.value());
				  
				  if(log.isTraceEnabled())
					  log.trace("Key={}\nValue={}", r.key() != null?new String((byte[])r.key()):"<null>", new String((byte[])r.value()));
				  
				  prod.send(producerRecord, sinkCallback);
				  recordsSent ++ ;
			  };
			  
   	          if(log.isDebugEnabled())
		        	log.debug("Sent {} records of {}.", recordsSent, records.size());
			    
			    
			  } catch (Exception e) {
				  if (this.stopping) {
			            return;
			      }
				  log.error("Error while sending records: {}", e.getMessage());
				  if(faultTolerant) {
					  log.error("Conection will retried in {} milliseconds:", retryTimeout);
					  try {
						  Thread.sleep(retryTimeout);

						} catch (InterruptedException e1) {
							log.error("Error while waiting for retry: {}", e1.getMessage());
						} finally {
							this.closeProducer();
						}
				  } else
					  throw e;
			  }
	}
	
}
