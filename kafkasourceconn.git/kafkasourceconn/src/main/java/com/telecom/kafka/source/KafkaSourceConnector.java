package com.telecom.kafka.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.connect.connector.Task;
import org.apache.kafka.connect.source.SourceConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaSourceConnector extends SourceConnector {

	public static final String VERSION = "1.0a";
	private static final Logger log = LoggerFactory.getLogger(KafkaSourceConnector.class);
	
	
	List<String> bootStrapServers = new ArrayList<String>();
	String clientId = "KafkaSourceConnector";
	String keyDeserializer = "org.apache.kafka.common.serialization.ByteArrayDeserializer";
	String valueDeserializer = "org.apache.kafka.common.serialization.ByteArrayDeserializer";

	String securityProtocol = "SASL_PLAINTEXT";
	String saslMechanism = "SCRAM-SHA-512";
	String saslJaasLoginModule = "org.apache.kafka.common.security.scram.ScramLoginModule";
	String sasJaaslUsername = null;
	String saslJaasPassword = null;
	
	String saslKerberosServiceName = null;
	
	String saslJaasConfig = null;
	
	String groupId = "KafkaSourceConnector-group";
	String topicRegex = null;
	
	String trustStoreLocation = null;
	String trustStorePassword = null;
	String trustStoreType = null;
	String topicSuffix = "";
	String topicPrefix = "";
	
	long pollTimeout = 5000L;
	long retryTimeout = pollTimeout*4;

	private String autoOffsetReset = "latest";
	private boolean faultTolerant = true;


//	public static void main(String[] args) throws FileNotFoundException, IOException {
//		
//		Properties p = new Properties();
//
//		List<KafkaSourceTask> kafkaSourceTasks = new ArrayList<KafkaSourceTask>();
//		List<Thread> threads = new ArrayList<Thread>();
//		
//		p.load(ClassLoader.getSystemResource("test.properties").openStream());
//		
//		Map<String, String> props = new HashMap<String, String>();
//		
//		for(Object i : p.keySet()) {
//			props.put(i.toString(), p.getProperty(i.toString()));
//		}
//		
//		KafkaSourceConnector connector = new KafkaSourceConnector();
//		
//		connector.start(props);
//		
//		List<Map<String, String>> taskConfigs = connector.taskConfigs(2);
//		
//		for(Map<String, String> tc : taskConfigs) {
//			KafkaSourceTask kst = new KafkaSourceTask();
//			kst.start(tc);
//			kafkaSourceTasks.add(kst);
//		}
//
//		for(KafkaSourceTask kst : kafkaSourceTasks) {
//			
//			Runnable task = new Runnable() {
//
//				@Override
//				public void run() {
//					int count = 0;
//					while(true) {
//						try {
//							List<SourceRecord> records = kst.poll();
//							for(SourceRecord record : records) {
//								System.out.println("key=%s, topic=%s, value=%s".formatted(record.key(), record.topic(), record.value().toString()));
//							};
//							count ++;
//							if(count >5) {
//								count = 0;
//								kst.commit();
//							}
//							Thread.sleep(1);
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//							break;
//						};
//					};
//				}
//			};
//			Thread thread = new Thread(task);
//			threads.add(thread);
//			thread.run();
//		}
//
//		for(Thread thread : threads) {
//			try {
//				thread.join();
//			} catch (InterruptedException e) {
//				System.err.println("Thread interrupted.");
//				e.printStackTrace();
//			}
//		}
//
//		for(KafkaSourceTask kst : kafkaSourceTasks) {
//			kst.stop();
//		}
//		
//	}

	@Override
	public String version() {
		return VERSION;
	}

	@Override
	public void start(Map<String, String> props) {

		bootStrapServers.clear();
		
		log.info("KAFKA SOURCE CONNECTOR CONFIG");
		for(Entry<String, String> keyval : props.entrySet()) {
			log.info("{} = {}", keyval.getKey(), keyval.getValue());
		}
		
		if(props.containsKey(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG)) {
			for(String bs : props.get(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG).split(",")) {
				bootStrapServers.add(bs.trim());
			}
		}

		keyDeserializer = props.getOrDefault(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeserializer);
		valueDeserializer = props.getOrDefault(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, valueDeserializer);

		securityProtocol = props.getOrDefault(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, securityProtocol);

		saslMechanism = props.getOrDefault(SaslConfigs.SASL_MECHANISM, saslMechanism);
		saslJaasConfig = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG, saslJaasConfig);
		saslKerberosServiceName = props.getOrDefault(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, saslKerberosServiceName);
		
		trustStoreLocation = props.getOrDefault(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
		trustStorePassword = props.getOrDefault(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
		trustStoreType = props.getOrDefault(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, trustStoreType);

		clientId = props.getOrDefault(CommonClientConfigs.CLIENT_ID_CONFIG, clientId);
		groupId = props.getOrDefault(CommonClientConfigs.GROUP_ID_CONFIG, groupId);
		autoOffsetReset  = props.getOrDefault(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);

		saslJaasLoginModule = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG + ".login-module", saslJaasLoginModule);

		topicRegex = props.getOrDefault("topics-regex", topicRegex);
		topicPrefix = props.getOrDefault("topics-prefix", topicPrefix);
		topicSuffix = props.getOrDefault("topics-suffix", topicSuffix);
		sasJaaslUsername = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG + ".username", sasJaaslUsername);
		saslJaasPassword = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG + ".password", saslJaasPassword);

		faultTolerant = Boolean.parseBoolean(props.getOrDefault("fault-tolerant", String.valueOf(faultTolerant)));

		try {
			pollTimeout = Long.parseLong(props.getOrDefault("poll-timeout", String.valueOf(pollTimeout)));
		} catch(NumberFormatException nfe) {
			log.error("Error: Value {} given for poll-timeout is invalid. Must be a long.", props.getOrDefault("poll-timeout", String.valueOf(pollTimeout)));
		}
		try {
			retryTimeout = Long.parseLong(props.getOrDefault("retry-timeout", String.valueOf(retryTimeout)));
		} catch(NumberFormatException nfe) {
			log.error("Error: Value {} given for retry-timeout is invalid. Must be a long.", props.getOrDefault("retry-timeout", String.valueOf(retryTimeout)));
		}		
		
	}

	@Override
	public Class<? extends Task> taskClass() {
		return KafkaSourceTask.class;
	}

	@Override
	public List<Map<String, String>> taskConfigs(int maxTasks) {
		List<Map<String, String>> configs = new ArrayList<Map<String, String>>();
		Map<String, String> config = new HashMap<String, String>();
		
		String bsConfig= "";
		for(String bs : bootStrapServers) {
			bsConfig += bs + ","; 
		};
		if(bsConfig.endsWith(","))
			bsConfig = bsConfig.substring(0, bsConfig.length()-1);
		
		config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bsConfig);
		
		config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeserializer);
		config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, valueDeserializer);
		 
		config.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, securityProtocol);

		config.put(SaslConfigs.SASL_MECHANISM, saslMechanism);

		if(saslJaasConfig == null) {
			String jaasConfig = String.format("%s required username=\"%s\" password=\"%s\";", saslJaasLoginModule, sasJaaslUsername, saslJaasPassword);
			config.put(SaslConfigs.SASL_JAAS_CONFIG, jaasConfig);
		} else 
			config.put(SaslConfigs.SASL_JAAS_CONFIG, saslJaasConfig);
		
		if(saslKerberosServiceName != null)
			config.put(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, saslKerberosServiceName);
		
		if(trustStoreLocation != null)
			config.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
		if(trustStorePassword != null)
			config.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
		if(trustStoreLocation != null && trustStoreType != null)
			config.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, trustStoreType);

		config.put(CommonClientConfigs.GROUP_ID_CONFIG, groupId);

		config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
		config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);

		config.put("topics-regex",topicRegex);
		config.put("topics-suffix",topicSuffix);
		config.put("topics-prefix",topicPrefix);

		config.put("poll-timeout", String.valueOf(pollTimeout));
		config.put("retry-timeout", String.valueOf(retryTimeout));
		config.put("fault-tolerant", String.valueOf(faultTolerant));
		
		for(int t=0; t < maxTasks; t++) {
			Map<String, String> taskConfig = new HashMap<String, String>(config);
			taskConfig.put(CommonClientConfigs.CLIENT_ID_CONFIG, String.format("%s-%d", clientId, t));
			configs.add(taskConfig);
		};
		return configs;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ConfigDef config() {
		ConfigDef configDef = ConsumerConfig.configDef();
				
//		configDef.define(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, Type.LIST,  ConfigDef.NO_DEFAULT_VALUE, Importance.HIGH, CommonClientConfigs.BOOTSTRAP_SERVERS_DOC);
//		
//		configDef.define(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, Type.STRING, securityProtocol, Importance.HIGH, CommonClientConfigs.SECURITY_PROTOCOL_DOC);
//
//		configDef.define(SaslConfigs.SASL_MECHANISM, Type.STRING, saslMechanism, Importance.LOW, SaslConfigs.SASL_MECHANISM_DOC);
//		configDef.define(SaslConfigs.SASL_JAAS_CONFIG, Type.STRING, saslJaasConfig, Importance.HIGH, SaslConfigs.SASL_JAAS_CONFIG_DOC);
//		configDef.define(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, Type.STRING, saslKerberosServiceName, Importance.LOW, SaslConfigs.SASL_KERBEROS_SERVICE_NAME_DOC);
//
//		configDef.define(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, Type.STRING, trustStoreLocation, Importance.LOW, SslConfigs.SSL_TRUSTSTORE_LOCATION_DOC);
//		configDef.define(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, Type.STRING, trustStorePassword, Importance.LOW, SslConfigs.SSL_TRUSTSTORE_PASSWORD_DOC);
//		configDef.define(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, Type.STRING, trustStoreType, Importance.LOW, SslConfigs.SSL_TRUSTSTORE_TYPE_DOC);
//
//		configDef.define(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, Type.STRING, keySerializer, Importance.LOW, ConsumerConfig.KEY_DESERIALIZER_CLASS_DOC);
//		configDef.define(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, Type.STRING, valueSerializer, Importance.LOW, ConsumerConfig.VALUE_DESERIALIZER_CLASS_DOC);
//		configDef.define(CommonClientConfigs.CLIENT_ID_CONFIG, Type.STRING, clientId, Importance.MEDIUM,CommonClientConfigs.CLIENT_ID_DOC);
//		configDef.define(CommonClientConfigs.GROUP_ID_CONFIG, Type.STRING, groupId, Importance.LOW, CommonClientConfigs.GROUP_ID_DOC);
//
//		configDef.define(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, Type.STRING, autoOffsetReset, Importance.LOW, ConsumerConfig.AUTO_OFFSET_RESET_DOC);

		configDef.define("topics-regex", Type.STRING, topicRegex, Importance.LOW, "Topics Regex");
		configDef.define("topics-prefix", Type.STRING, topicPrefix, Importance.LOW, "Topic Prefix on destination.");
		configDef.define("topics-suffix", Type.STRING, topicSuffix, Importance.LOW, "Topic Suffix on destination.");
		configDef.define("poll-timeout", Type.LONG, pollTimeout, Importance.LOW, "Poll timeout.");
		configDef.define("retry-timeout", Type.LONG, retryTimeout, Importance.LOW, "Error retry timeout.");
		configDef.define("fault-tolerant", Type.BOOLEAN, faultTolerant , Importance.LOW, "Tolerance to faults.");
		
		configDef.define(SaslConfigs.SASL_JAAS_CONFIG + ".login-module", Type.STRING, saslJaasLoginModule, Importance.LOW, "SASL JAAS Login module");
		configDef.define(SaslConfigs.SASL_JAAS_CONFIG + ".username", Type.STRING, sasJaaslUsername, Importance.LOW, "SASL Username");
		configDef.define(SaslConfigs.SASL_JAAS_CONFIG + ".password", Type.STRING, saslJaasPassword, Importance.LOW, "SASL Password");

		configDef.define("probability", Type.DOUBLE, 1.0d, Importance.LOW, "Probability of sending a message.");

		return configDef;
	}

}
