package com.telecom.kafka.sink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.connect.connector.Task;
import org.apache.kafka.connect.sink.SinkConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaSinkConnector extends SinkConnector {

	public static final String VERSION = "1.0a";
	private static final Logger log = LoggerFactory.getLogger(KafkaSinkConnector.class);
	
	
	List<String> bootStrapServers = new ArrayList<String>();
	String clientId = "KafkaSinkConnector";
	String keySerializer = "org.apache.kafka.common.serialization.ByteArraySerializer";
	String valueSerializer = "org.apache.kafka.common.serialization.ByteArraySerializer";

	org.apache.kafka.common.serialization.ByteArraySerializer ss;
	String securityProtocol = "SASL_PLAINTEXT";
	String saslMechanism = "SCRAM-SHA-512";
	String saslJaasLoginModule = "org.apache.kafka.common.security.scram.ScramLoginModule";
	String sasJaaslUsername = null;
	String saslJaasPassword = null;
	
	String saslKerberosServiceName = null;
	
	String saslJaasConfig = null;
	
	// String groupId = "KafkaSinkConnector-group";
	// String topicRegex = null;
	
	String trustStoreLocation = null;
	String trustStorePassword = null;
	String trustStoreType = null;
	String topicSuffix = "";
	String topicPrefix = "";
	Boolean enableProducerIdempotence = true;
	
//	long pollTimeout = 5000L;
	long retryTimeout = 20000L;

	private boolean faultTolerant = true;

	@Override
	public String version() {
		return VERSION;
	}

	@Override
	public void start(Map<String, String> props) {

		bootStrapServers.clear();
		
		log.info("KAFKA SINK CONNECTOR CONFIG");
		for(Entry<String, String> keyval : props.entrySet()) {
			log.info("{} = {}", keyval.getKey(), keyval.getValue());
		}
		
		if(props.containsKey(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG)) {
			for(String bs : props.get(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG).split(",")) {
				bootStrapServers.add(bs.trim());
			}
		}

		keySerializer = props.getOrDefault(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
		valueSerializer = props.getOrDefault(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);

		securityProtocol = props.getOrDefault(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, securityProtocol);

		saslMechanism = props.getOrDefault(SaslConfigs.SASL_MECHANISM, saslMechanism);
		saslJaasConfig = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG, saslJaasConfig);
		saslKerberosServiceName = props.getOrDefault(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, saslKerberosServiceName);
		
		trustStoreLocation = props.getOrDefault(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
		trustStorePassword = props.getOrDefault(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
		trustStoreType = props.getOrDefault(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, trustStoreType);

		clientId = props.getOrDefault(CommonClientConfigs.CLIENT_ID_CONFIG, clientId);
		// groupId = props.getOrDefault(CommonClientConfigs.GROUP_ID_CONFIG, groupId);
		//autoOffsetReset  = props.getOrDefault(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);

		saslJaasLoginModule = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG + ".login-module", saslJaasLoginModule);

		// topicRegex = props.getOrDefault("topics-regex", topicRegex);
		topicPrefix = props.getOrDefault("topics-prefix", topicPrefix);
		topicSuffix = props.getOrDefault("topics-suffix", topicSuffix);
		sasJaaslUsername = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG + ".username", sasJaaslUsername);
		saslJaasPassword = props.getOrDefault(SaslConfigs.SASL_JAAS_CONFIG + ".password", saslJaasPassword);

		faultTolerant = Boolean.parseBoolean(props.getOrDefault("fault-tolerant", String.valueOf(faultTolerant)));

		enableProducerIdempotence = Boolean.parseBoolean(props.getOrDefault("enable-producer-idempotence", String.valueOf(enableProducerIdempotence)));
//		try {
//			pollTimeout = Long.parseLong(props.getOrDefault("poll-timeout", String.valueOf(pollTimeout)));
//		} catch(NumberFormatException nfe) {
//			log.error("Error: Value {} given for poll-timeout is invalid. Must be a long.", props.getOrDefault("poll-timeout", String.valueOf(pollTimeout)));
//		}
		try {
			retryTimeout = Long.parseLong(props.getOrDefault("retry-timeout", String.valueOf(retryTimeout)));
		} catch(NumberFormatException nfe) {
			log.error("Error: Value {} given for retry-timeout is invalid. Must be a long.", props.getOrDefault("retry-timeout", String.valueOf(retryTimeout)));
		}
		
		
	}

	@Override
	public Class<? extends Task> taskClass() {
		return KafkaSinkTask.class;
	}

	@Override
	public List<Map<String, String>> taskConfigs(int maxTasks) {
		List<Map<String, String>> configs = new ArrayList<Map<String, String>>();
		Map<String, String> config = new HashMap<String, String>();
		
		String bsConfig= "";
		for(String bs : bootStrapServers) {
			bsConfig += bs + ","; 
		};
		if(bsConfig.endsWith(","))
			bsConfig = bsConfig.substring(0, bsConfig.length()-1);
		
		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bsConfig);
		
		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
		 
		config.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, securityProtocol);

		config.put(SaslConfigs.SASL_MECHANISM, saslMechanism);

		if(saslJaasConfig == null) {
			String jaasConfig = String.format("%s required username=\"%s\" password=\"%s\";", saslJaasLoginModule, sasJaaslUsername, saslJaasPassword);
			config.put(SaslConfigs.SASL_JAAS_CONFIG, jaasConfig);
		} else 
			config.put(SaslConfigs.SASL_JAAS_CONFIG, saslJaasConfig);
		
		if(saslKerberosServiceName != null)
			config.put(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, saslKerberosServiceName);
		
		if(trustStoreLocation != null)
			config.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
		if(trustStorePassword != null)
			config.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
		if(trustStoreLocation != null && trustStoreType != null)
			config.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, trustStoreType);

		// config.put(CommonClientConfigs.GROUP_ID_CONFIG, groupId);

//		config.put(ProducerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
//		config.put(ProducerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);

		// config.put("topics-regex",topicRegex);
		config.put("topics-suffix",topicSuffix);
		config.put("topics-prefix",topicPrefix);

//		config.put("poll-timeout", String.valueOf(pollTimeout));
		config.put("retry-timeout", String.valueOf(retryTimeout));
		config.put("fault-tolerant", String.valueOf(faultTolerant));
		config.put("enable-producer-idempotence", String.valueOf(this.enableProducerIdempotence));
		
		for(int t=0; t < maxTasks; t++) {
			Map<String, String> taskConfig = new HashMap<String, String>(config);
			taskConfig.put(CommonClientConfigs.CLIENT_ID_CONFIG, String.format("%s-%d", clientId, t));
			configs.add(taskConfig);
		};
		return configs;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ConfigDef config() {
		ConfigDef configDef = ProducerConfig.configDef();

		configDef.define("retry-timeout", Type.LONG, retryTimeout, Importance.LOW, "Error retry timeout.");
		configDef.define("topics-prefix", Type.STRING, topicPrefix, Importance.LOW, "Topic Prefix on destination.");
		configDef.define("topics-suffix", Type.STRING, topicSuffix, Importance.LOW, "Topic Suffix on destination.");

		configDef.define("fault-tolerant", Type.BOOLEAN, faultTolerant , Importance.LOW, "Tolerance to faults.");
		configDef.define("enable-producer-idempotence", Type.BOOLEAN, faultTolerant , Importance.LOW, "Enables/Disables producer idempotence");
		
		
		configDef.define(SaslConfigs.SASL_JAAS_CONFIG + ".login-module", Type.STRING, saslJaasLoginModule, Importance.LOW, "SASL JAAS Login module");
		configDef.define(SaslConfigs.SASL_JAAS_CONFIG + ".username", Type.STRING, sasJaaslUsername, Importance.LOW, "SASL Username");
		configDef.define(SaslConfigs.SASL_JAAS_CONFIG + ".password", Type.STRING, saslJaasPassword, Importance.LOW, "SASL Password");

		return configDef;
	}

}
