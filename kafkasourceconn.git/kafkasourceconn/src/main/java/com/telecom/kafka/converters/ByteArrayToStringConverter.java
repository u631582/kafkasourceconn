package com.telecom.kafka.converters;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.utils.Utils;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaAndValue;
import org.apache.kafka.connect.errors.DataException;
import org.apache.kafka.connect.storage.Converter;
import org.apache.kafka.connect.storage.ConverterType;
import org.apache.kafka.connect.storage.HeaderConverter;
import org.apache.kafka.connect.storage.StringConverterConfig;

public class ByteArrayToStringConverter implements Converter, HeaderConverter {
    
	private final StringSerializer serializer = new StringSerializer();
    private final ByteArrayDeserializer deserializer = new ByteArrayDeserializer();
	private String encoding = StandardCharsets.UTF_8.name();
    
    
    public ByteArrayToStringConverter() {
    	
    }
    
	@Override
	public void close() throws IOException {
        Utils.closeQuietly(this.serializer, "string converter serializer");
        Utils.closeQuietly(this.deserializer, "byte array converter deserializer");
	}

	@Override
	public SchemaAndValue toConnectHeader(String topic, String headerKey, byte[] value) {
		return toConnectData(topic, value);
	}

	@Override
	public byte[] fromConnectHeader(String topic, String headerKey, Schema schema, Object value) {
		return fromConnectData(topic, schema, value);
	}

	@Override
	public ConfigDef config() {
		return StringConverterConfig.configDef();
	}

	@Override
	public void configure(Map<String, ?> configs) {
        StringConverterConfig conf = new StringConverterConfig(configs);
        this.encoding = conf.encoding();

        Map<String, Object> serializerConfigs = new HashMap<>(configs);
        Map<String, Object> deserializerConfigs = new HashMap<>(configs);
        serializerConfigs.put("serializer.encoding", encoding);
        deserializerConfigs.put("deserializer.encoding", encoding);

        boolean isKey = conf.type() == ConverterType.KEY;
        serializer.configure(serializerConfigs, isKey);
        deserializer.configure(deserializerConfigs, isKey);

	}

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {
		
        Map<String, Object> conf = new HashMap<>(configs);
        conf.put(StringConverterConfig.TYPE_CONFIG, isKey ? ConverterType.KEY.getName() : ConverterType.VALUE.getName());
        configure(conf);
		
	}

	@Override
	public byte[] fromConnectData(String topic, Schema schema, Object value) {
        try {
        	if(value instanceof byte[])
        		return serializer.serialize(topic, value == null ? null : new String((byte[]) value , encoding));
        	else
        		return serializer.serialize(topic, value == null ? null : value.toString());

        } catch (SerializationException | UnsupportedEncodingException e) {
            throw new DataException("Failed to serialize to a string: ", e);
        }
	}

	@Override
	public SchemaAndValue toConnectData(String topic, byte[] value) {
        try {
        	
        	String str = new String(deserializer.deserialize(topic, value), encoding);

            return new SchemaAndValue(Schema.STRING_SCHEMA, str);
            
        } catch (SerializationException | UnsupportedEncodingException e) {
            throw new DataException("Failed to deserialize ByteArray: ", e);
        }
    }

}
